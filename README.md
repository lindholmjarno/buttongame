# Simple button game

Made with Next.js and Tailwindcss

# Installation

Application needs Nodejs and npm or yarn to be installed to system.

1. git clone repo
2. cd buttongame/
3. npm install
4. run npm script
  - if dev: npm run dev
  - if prod: npm run build && npm run start
5. Go to http://localhost:3000/
