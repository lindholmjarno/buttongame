let counter = 24198;

export default (req, res) => {
  let reward = 0;
  let clicksForReward = 0;

  counter++;

  if (counter % 500 === 0) {
    reward = 250;
  } else if (counter % 100 === 0) {
    reward = 40;
  } else if (counter % 10 === 0) {
    reward = 5;
  } else {
    reward = 0;
    // When taking modulus 10 from number it gives last digit
    // and substract it from 10 to get how many clicks are needed
    clicksForReward = 10 - (counter % 10);
  }
  res.status(200).json({ reward, clicksForReward });
};
