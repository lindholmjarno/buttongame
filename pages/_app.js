import React, { useState } from "react";
import axios from "axios";
import Header from "../Components/Header";
import Footer from "../Components/Footer";

import "../css/tailwind.css";

const App = () => {
  const [score, setScore] = useState();
  const [award, setAward] = useState(0);
  const [clicksNeeded, setClicksNeeded] = useState(0);

  React.useEffect(() => {
    setScore(Number(localStorage.getItem("score")) || 20);
  }, []);

  React.useEffect(() => {
    localStorage.setItem("score", String(score));
  }, [score]);

  const handlePlayButtonClick = () => {
    let tmpScore = score - 1;
    axios
      .get("/api/getCounter")
      .then(response => {
        const { reward, clicksForReward } = response.data;
        setAward(reward);
        setScore(tmpScore + reward);
        setClicksNeeded(clicksForReward);
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  const handleResetButtonClick = () => {
    setScore(20);
  };

  return (
    <div className="h-screen flex flex-col">
      <Header award={award} clicksNeeded={clicksNeeded} />
      <div className="flex-grow flex items-center justify-center text-center bg-grey-lighter">
        <button
          disabled={score < 1}
          className="bg-blue-500 hover:bg-blue-700 text-center text-white h-32 w-32 items-center justify-center font-bold py-2 px-4 rounded-full"
          onClick={() => handlePlayButtonClick()}
        >
          {score}
        </button>
      </div>
      {score === 0 && (
        <button
          className="bg-red-500 hover:bg-red-700 text-white font-bold h-24 w-24 flex items-center justify-center mx-4 my-4 py-2 px-4 rounded-full"
          onClick={() => handleResetButtonClick()}
        >
          Reset your balance
        </button>
      )}
      <Footer />
    </div>
  );
};

export default App;
