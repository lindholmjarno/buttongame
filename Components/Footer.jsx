import React from "react";

const Footer = () => {
  return (
    <footer className="bg-indigo-900 w-full text-center text-indigo-100 p-4">
      <span className="font-semibold mr-2">
        <a href="https://gitlab.com/lindholmjarno/buttongame">Source code</a>
      </span>
    </footer>
  );
};
export default Footer;
