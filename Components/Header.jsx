import React from "react";

const Header = ({ award, clicksNeeded }) => {
  return (
    <div className="bg-indigo-900 h-16 text-center py-4 lg:px-4">
      <div
        className="p-2 bg-indigo-800 items-center text-indigo-100 leading-none rounded-full flex inline-flex"
        role="alert"
      >
        <span className="rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">
          Needs
        </span>
        <span className="font-semibold mr-2 text-left flex-auto ">
          {clicksNeeded} clicks
        </span>

        <div className={award === 0 ? "invisible" : "fadeOut"}>
          <span className="rounded-full bg-indigo-500 uppercase px-2 py-1 text-xs font-bold mr-3">
            Win
          </span>
          <span className="font-semibold mr-2 text-left flex-auto ">
            + {award} points
          </span>
        </div>
      </div>
    </div>
  );
};

export default Header;
